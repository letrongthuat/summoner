package com.example.summoner;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


public class MainActivity extends Activity {

	private SlidingMenu slidingMenu;
	ImageView btnSliding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        btnSliding  = (ImageView)findViewById(R.id.btn_sliding);
     // add left slide menu
     		slidingMenu = new SlidingMenu(this.getApplicationContext());
     		slidingMenu.setMode(SlidingMenu.LEFT);
     		
     		slidingMenu.setTouchModeBehind(SlidingMenu.TOUCHMODE_MARGIN);
     		
     		slidingMenu.setShadowWidth(30);
     		slidingMenu.setFadeDegree(0.0f);
     		slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
	
     		slidingMenu.setBehindWidth(420);
     		slidingMenu.setMenu(R.layout.slide_menu);
        btnSliding.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				slidingMenu.toggle();
			}
		});
     		

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
